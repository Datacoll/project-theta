# APTOS ROCK PAPER SCISSORS LIZARD SPOCK GAME DAPP
## Introduction
Dive into the Rock Paper Scissors Lizard Spock game on the Aptos blockchain! This game has been built to enhance the traditional Rock Paper Scissors by introducing two extra options: Lizard and Spock, ensuring a more enjoyable and captivating experience.

## Navigation Links

- live site: <a href="https://rockpaperscissorslizardspockapt.netlify.app/">Play Rock Paper Scissors Lizard Spock</a>
- demo: <a href="https://youtu.be/tIrnCPmCzhs">How to play</a>

## Additional Features

### Smart Contract Score Tracking
The game's smart contract, implemented in ``RockPaperScissorsLizardSpock.move``, handles score tracking through the `_GameResult_` struct:

```
struct GameResult has key {
    computer_move: String,
    game_result: String,
    player_score: u64,
    computer_score: u64,
    draws: u64
}
```
This struct stores the current game state and cumulative scores. The duel function updates these scores based on the game outcome:

Player wins: player_score is incremented.
Computer wins: computer_score is incremented.
Draw: draws is incremented.

### Endless Gameplay

### Frontend-Blockchain Interaction
1. Connecting to the blockchain: The frontend uses the Aptos client to connect. The Aptos object handle everything that requires a connection to the Aptos network. A connection is established as soon as the object created.
```
 const aptosConfig = new AptosConfig({ network: Network.TESTNET });
 const client = new Aptos(aptosConfig);
```
2. Fetching scores: The fetchScores function retrieves the current scores from the blockchain: 
```
const fetchScores = async () => {
     if (!account) return;
     try {
         const resource = await client.getAccountResource({
             accountAddress: account.address,
             resourceType: `${moduleAddress}::${moduleName}::GameResult`,
         });
         setScores({
             user: Number(resource.player_score),
             computer: Number(resource.computer_score),
             draws: Number(resource.draws),
         });
     } catch (error) {
         console.log("Error fetching scores: " + error);
     }
 };
```
The ``getAccountResource`` method is used to fetch the resource from the blockchain, which contains the scores.


3. Payload: The payload is an object that contains the data needed to execute a transaction on the Aptos blockchain:

```
 const payload: InputTransactionData = {
     data: {
         function: `${moduleAddress}::${moduleName}::duel`,
         functionArguments: [move],
     },
 };
```

4. signAndSubmitTransaction: This function is provided by the Aptos Wallet Adapter and is used to sign and submit the transaction to the Aptos network:
```const response = await signAndSubmitTransaction(payload);```
It takes the payload, signs it with the user's wallet, and submits it to the blockchain. This function handles the complexities of transaction signing and submission.
